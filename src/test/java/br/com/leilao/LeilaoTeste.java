package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {

    private Usuario usuario;
    private Usuario usuario2;
    private Lance lanceMaior;
    private Lance lanceMenor;
    private Leilao leilao = new Leilao();
    private Leiloeiro leiloeiro = new Leiloeiro ("Jonas", leilao);



    @BeforeEach
    public void setUp(){
        this.usuario = new Usuario(1,"Maria");
        this.usuario2 = new Usuario(2,"Marcos");
        this.lanceMaior = new Lance(usuario,200.00);
        this.lanceMenor = new Lance(usuario2,150.00);
    }

    @Test
    public void testarNovoLance(){
        leilao.adicionarLance(lanceMenor);
        Assertions.assertEquals(true, leilao.getLances().contains(lanceMenor));
    }

    @Test
    public void testarLanceValorMenor(){
        leilao.adicionarLance(lanceMaior);
        Assertions.assertThrows(RuntimeException.class,() -> {leilao.adicionarLance(lanceMenor);});
    }

    @Test
    public void testarMaiorLanceNoLeilao(){
        leilao.adicionarLance(lanceMenor);
        leilao.adicionarLance(lanceMaior);
        Assertions.assertEquals(lanceMaior.getValorDoLance(), leiloeiro.retornarMaiorLanceLeilao().getValorDoLance());
        Assertions.assertEquals(lanceMaior.getUsuario(), leiloeiro.retornarMaiorLanceLeilao().getUsuario());
    }

}
